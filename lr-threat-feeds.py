#!/bin/python

# lr-threat-feeds.py
# Version 0.3, 8/24/17
# Developed for Fairview Health Services - www.fairview.org
# by Chris Goff (cgoff1@fairview.org)

from netaddr import *
from urllib.request import urlretrieve
from openpyxl import load_workbook
import re
import os

################
# INSTRUCTIONS #
################
# Create a new list in LogRhythm, and specifiy the file names you need
# Create a scheduled task on your AIE system that calls this script
# The LogRhythm Job Manager will automatically update the lists

# Set path to LR list import folder
path = "C:/Program Files/LogRhythm/LogRhythm Job Manager/config/list_import"

# Regular expressions to match an IP address
is_ip = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")

# Regular expressions to match domain name
is_domain = re.compile("^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$")

# Import HSIN threat feed
wb = load_workbook('IPs of Interest 08-04-17 to 08-10-17.xlsx')

sheet1 = wb.get_sheet_by_name('Malware IP')
sheet2 = wb.get_sheet_by_name('Malware Domains')

try:
	hsin-threats-by-ip = open(os.path.join(path, "hsin-threats-by-ip.txt"), "w")
	for col in sheet1.iter_cols(max_col=1):
		for cell in col:
			if is_ip.match(cell.value):
				print(cell.value, file=hsin-threats-by-ip)

except:
	hsin-threats-by-domain = open(os.path.join(path, "hsin-threats-by-domain.txt"), "w")
	for col in sheet2.iter_cols(max_col=1):
		for cell in col:
			if is_domain.match(cell.value):
				print(cell.value, file=hsin-threats-by-domain)

# https://iplists.firehol.org/
urlretrieve('https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level1.netset', 'firehol_level1.netset')

# https://ransomwaretracker.abuse.ch/blocklist/
urlretrieve('https://ransomwaretracker.abuse.ch/downloads/RW_DOMBL.txt', 'RW_DOMBL.tmp')
urlretrieve('https://ransomwaretracker.abuse.ch/downloads/RW_URLBL.txt', 'RW_URLBL.tmp')
urlretrieve('https://ransomwaretracker.abuse.ch/downloads/RW_IPBL.txt', 'RW_IPBL.tmp')

# Open the threat feed
raw_file = open('firehol_level1.netset', "r")

# Designate the files to write the formatted IP addresses and ranges to
threat_list = open(os.path.join(path, "FireHOL.org.IPs.txt"), "w")
threat_list_ranges = open("ip-ranges.tmp", "w")

# Logic to extract and format the IP addresses
for line in raw_file:
	li = line.strip()
	if not li.startswith("#"):
		if "/" in li:
			s1 = IPSet([line])
			print(s1.iprange(), file=threat_list_ranges)
		elif is_ip.match(li):
			print(li, file=threat_list)

read_temporary_file = open("ip-ranges.tmp", "r")
write_formatted_strings = open(os.path.join(path, "FireHOL.org.subnets.txt"), "w")

for line in read_temporary_file:
	this_string = line.replace('-', '~')
	write_formatted_strings.write(this_string)

with open('RW_DOMBL.tmp', 'r') as RW_DOMBL:
	DOMBL_formatted = open(os.path.join(path, 'ransomware-tracker-dombl.txt'), 'w')
	for line in RW_DOMBL:
		li = line.strip()
		if not li.startswith("#"):
				print(li, file=DOMBL_formatted)

with open('RW_URLBL.tmp', 'r') as RW_URLBL:
	URLBL_formatted = open(os.path.join(path, 'ransomware-tracker-urlbl.txt'), 'w')
	for line in RW_URLBL:
		li = line.strip()
		if not li.startswith("#"):
				print(li, file=URLBL_formatted)

with open('RW_IPBL.tmp', 'r') as RW_IPBL:
	IPBL_formatted = open(os.path.join(path, 'ransomware-tracker-ipbl.txt'), 'w')
	for line in RW_IPBL:
		li = line.strip()
		if not li.startswith("#"):
				print(li, file=IPBL_formatted)

# Cleanup
raw_file.close()
threat_list.close()
threat_list_ranges.close()
read_temporary_file.close()
write_formatted_strings.close()
hsin-threats-by-ip.close()
hsin-threats-by-domain.close()
os.remove('firehol_level1.netset')
os.remove('ip-ranges.tmp')
os.remove('RW_DOMBL.tmp')
os.remove('RW_URLBL.tmp')
os.remove('RW_IPBL.tmp')