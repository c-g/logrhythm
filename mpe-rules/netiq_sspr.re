# NetIQ SSPR Event Regular Expression for LogRhythm (tested 7.2.6)

# "SSPR CATCH_ALL"
^.*?(<dip>)\s<(USER):(<severity>)>.*?{"\w+":"(<account>)","\w+".*?"sourceAddress":"(<sip>)","sourceHost":"((?<sname>.*?))".*?"eventCode":"(<command>)".*?"xdasTaxonomy":"(<object>)","xdasOutcome":"(<result>)"

# "SSPR INTRUDER_ATTEMPT"
^.*?"eventCode":"(<command>)".*?"(subject\\":\\"((?<sname>.*?))\\").*?"xdasTaxonomy":"(<object>)","xdasOutcome":"(<result>)"