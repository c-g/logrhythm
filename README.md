# Collection of custom tooling for LogRhythm

*NOTE: I do not work for nor represent [LogRhythm](https://www.logrhythm.com)*. I'm just an engineer happy with their product.

## maintenance-scripts/lr-service-monitor.ps1
PowerShell script which monitors the state (`UnprocessedLogs` and `UnprocessedArchives`) of the Mediator it is executed on. Acts like a process (simply modify the variables under the CONFIGURATION section as necessary and execute). May require running under a privileged account.

When one or more of the state folders has more than X (default: 25) files contained, the script will stop the Mediator service, rename the existing state folder(s), restart the Mediator service (to resume processing), and it will feed the orphaned files back into the processor.

At this point I have chosen to not include `DXReliablePersist` monitoring. If this starts filling it is indicative of another issue. Changing the Mediator service status probably won't affect this issue in a meaningful way.

## mpe-rules/protectwise.re
Regular expression for ProtectWise events.
Make sure you have your protectwse JSON emitter (`protectwise-emitter.json`) configured for SYSLOG output.

## mpe-rules/netiq_sspr.re
Collection of regular expressions for NetIQ's Single Sign-On Password Reset (SSPR) tool.

## smart-response-plugins/force-logoff
Smart Response Plug-in which forces all users to logoff. The use case for this is if X process fails, execute this plug-in on the kiosk which forces the process to restart.

## lr-threat-feeds.py [DEPRECATED]
The Poor Man's Threat Intelligence Feed Importer for LogRhythm. Note this is superceded by the Threat Intelligence module available from LogRhythm. Keeping for the memories or one off's where the aforementioned module isn't flexible enough.

Usage: `python lr-threat-feeds.py`

Dependencies: `netaddr` module


### Setup
The script is setup to run on the LogRhythm system containing the LogRhythm Job Manager. It can be run from anywhere that can talk to the LogRhythm Job Manager, just modify the path variable as needed.

If you do not have lists already created in LogRhythm, you will need to do so and specify the text file names in the list import.

Schedule `lr-threat-feeds.py` with Windows Task Scheduler.

### Feeds
- **firehol_level1**: http://iplists.firehol.org/
- **Abuse.ch Ransomware**: https://ransomwaretracker.abuse.ch/

Due to the way LogRhythm lists work, we cannot have a combined IP and IP range list. This script creates two LogRhythm list format compatible text files, one for single IPs, and another for IP ranges. Note that LogRhythm requires a tilde (~) to denote ranges (e.g 192.168.1.1~192.168.2.254).